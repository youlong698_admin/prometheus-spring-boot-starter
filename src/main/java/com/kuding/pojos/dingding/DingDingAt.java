package com.kuding.pojos.dingding;

public class DingDingAt {

	private String[] atMobiles;

	public DingDingAt(String... atMobiles) {
		this.atMobiles = atMobiles;
	}

	/**
	 * @return the atMobiles
	 */
	public String[] getAtMobiles() {
		return atMobiles;
	}

	/**
	 * @param atMobiles
	 *            the atMobiles to set
	 */
	public void setAtMobiles(String[] atMobiles) {
		this.atMobiles = atMobiles;
	}

}
